package com.dbsoftwares.scoreboard.placeholder;

import com.dbsoftwares.centrixcore.api.CCore;
import com.dbsoftwares.centrixcore.api.placeholders.PlaceHolderAPI;
import com.dbsoftwares.centrixcore.api.placeholders.PlaceHolderEvent;
import com.dbsoftwares.centrixcore.api.placeholders.PlaceHolderEventHandler;
import com.dbsoftwares.centrixcore.api.placeholders.PlaceHolderPack;
import com.dbsoftwares.centrixcore.api.server.ServerInfo;
import com.dbsoftwares.scoreboard.ScoreboardManager;

import java.util.List;
import java.util.Map;

public class ServerPlaceholderPack implements PlaceHolderPack {

    @Override
    public void loadPack() {
        ScoreboardManager extension = ScoreboardManager.getPlugin(ScoreboardManager.class);

        for (Map.Entry<String, List<String>> entry : extension.getServers().entrySet()) {
            PlaceHolderAPI.addPlaceHolder("%" + entry.getKey() + "%", new PlaceHolderEventHandler() {
                @Override
                public String getReplace(PlaceHolderEvent event) {
                    Integer count = 0;
                    for (String server : entry.getValue()) {
                        ServerInfo info = CCore.getApi().getOrCreateServer(server);

                        count += info.getCount();
                    }
                    return count.toString();
                }
            });
        }
    }
}
