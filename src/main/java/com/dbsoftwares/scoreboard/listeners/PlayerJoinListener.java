package com.dbsoftwares.scoreboard.listeners;

import com.dbsoftwares.scoreboard.ScoreboardManager;
import com.dbsoftwares.scoreboard.scoreboard.Scoreboard;
import com.dbsoftwares.scoreboard.scoreboard.ScoreboardSettings;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        ScoreboardManager inst = ScoreboardManager.getPlugin(ScoreboardManager.class);

        if (!inst.scoreboardOnJoin) {
            return;
        }
        Scoreboard scoreboard = new Scoreboard(event.getPlayer(), ScoreboardSettings.getDefault());
        scoreboard.buildScoreboard();
    }
}