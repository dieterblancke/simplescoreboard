package com.dbsoftwares.scoreboard.listeners;

import com.dbsoftwares.scoreboard.ScoreboardManager;
import com.dbsoftwares.scoreboard.scoreboard.Scoreboard;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        unregister(event.getPlayer());
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        unregister(event.getPlayer());
    }

    private void unregister(Player p) {
        ScoreboardManager instance = ScoreboardManager.getInstance();
        if (!instance.getScoreboards().containsKey(p.getName())) {
            return;
        }
        Scoreboard scoreboard = instance.getScoreboards().get(p.getName());
        scoreboard.unregister();
    }
}