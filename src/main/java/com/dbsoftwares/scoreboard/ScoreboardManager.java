package com.dbsoftwares.scoreboard;

import com.dbsoftwares.core.api.configuration.IConfiguration;
import com.dbsoftwares.core.api.configuration.json.JsonConfiguration;
import com.dbsoftwares.scoreboard.listeners.PlayerJoinListener;
import com.dbsoftwares.scoreboard.listeners.PlayerQuitListener;
import com.dbsoftwares.scoreboard.placeholder.ServerPlaceholderPack;
import com.dbsoftwares.scoreboard.runnable.ScoreboardRunnable;
import com.dbsoftwares.scoreboard.scoreboard.Scoreboard;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class ScoreboardManager extends JavaPlugin {

    @Getter
    private static ScoreboardManager instance;

    public Boolean scoreboardOnJoin = true;

    @Getter
    private ConcurrentMap<String, Scoreboard> scoreboards = Maps.newConcurrentMap();

    @Getter
    private Map<String, List<String>> servers = Maps.newHashMap();

    @Override
    public void onEnable() {
        instance = this;

        onReload();
        PlaceHolderAPI.loadPlaceHolderPack(new ServerPlaceholderPack());

        getServer().getPluginManager().registerEvents(this, new PlayerJoinListener());
        getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);

        getServer().getScheduler().runTaskTimer(this, new ScoreboardRunnable(), 20, 20);
    }

    @Override
    public void onDisable() {
        List<Scoreboard> toUnregister = Lists.newArrayList(scoreboards.values());
        toUnregister.forEach(Scoreboard::unregister);

        toUnregister.clear();
    }

    public void onReload() {
        File file = new File(getDataFolder(), "config.json");

        if(!file.exists()) {
            saveResource("config.json", true);
        }
        JsonConfiguration config = IConfiguration.loadConfiguration(JsonConfiguration.class, file);

        for (String key : config.getKeys()) {
            if (key.equalsIgnoreCase("scoreboardOnJoin")) {
                scoreboardOnJoin = config.getBoolean("scoreboardOnJoin");
            } else {
                servers.put(key, config.getStringList(key));
            }
        }
        for (Language language : Language.values()) {
            File scoreboard = new File(getDataFolder(), language.toString().toLowerCase() + ".json");

            if(!scoreboard.exists()) {
                saveResource(language.toString().toLowerCase() + ".json", true);
            }
            JsonConfiguration langConfig = IConfiguration.loadConfiguration(JsonConfiguration.class, scoreboard);
            titles.put(language, langConfig.getString("title"));
            lines.put(language, Lists.newLinkedList(langConfig.getStringList("lines")));
        }
    }

    public LinkedList<String> getLines(Language language) {
        return lines.get(language);
    }

    public String getTitle(Language language) {
        return titles.get(language);
    }
}