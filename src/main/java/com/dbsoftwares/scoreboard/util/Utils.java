package com.dbsoftwares.scoreboard.util;

/*
 * Created by DBSoftwares on 08/01/2018
 * Developer: Dieter Blancke
 * Project: CExtensions
 */

import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import java.util.List;

public class Utils {

    public static List<String> colorList = Lists.newArrayList("§1§r", "§2§r", "§3§r", "§4§r",
                            "§5§r", "§6§r", "§7§r", "§8§r", "§9§r", "§a§r", "§c§r", "§b§r", "§d§r", "§e§r", "§f§r");

    public static String randomString(Integer length) {
        String text = "abcdefr0123456789";
        char[] chars = new char[length];

        for (int i = 0; i < length; i++) {
            chars[i] = text.charAt(MathUtils.randomRangeInt(0, text.length() - 1));
        }

        return new String(chars);
    }

    public static String colorizedString(String str) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            builder.append(ChatColor.translateAlternateColorCodes('&', "&" + String.valueOf(str.charAt(i))));
        }

        return builder.toString();
    }

    public static String randomColoredString(int length) {
        return colorizedString(randomString(length));
    }

    public static String[] splitString(String string) {
        StringBuilder prefix = new StringBuilder(string.substring(0, string.length() >= 16 ? 16 : string.length()));
        StringBuilder suffix = new StringBuilder(string.length() > 16 ? string.substring(16) : "");
        if (prefix.toString().length() > 1 && prefix.charAt(prefix.length() - 1) == '§') {
            prefix.deleteCharAt(prefix.length() - 1);
            suffix.insert(0, '§');
        }
        int length = prefix.length();
        boolean PASSED, UNDERLINE, STRIKETHROUGH, MAGIC, ITALIC;
        boolean BOLD = ITALIC = MAGIC = STRIKETHROUGH = UNDERLINE = PASSED = false;
        ChatColor textColor = null;
        for (int index = length - 1; index > -1; index--) {
            char section = prefix.charAt(index);
            if ((section == '§') && (index < prefix.length() - 1)) {
                char c = prefix.charAt(index + 1);
                ChatColor color = ChatColor.getByChar(c);
                if (color != null) {
                    if (color.equals(ChatColor.RESET)) {
                        break;
                    }
                    if ((textColor == null) && (color.isFormat())) {
                        if ((color.equals(ChatColor.BOLD)) && (!BOLD)) {
                            BOLD = true;
                        } else if ((color.equals(ChatColor.ITALIC)) && (!ITALIC)) {
                            ITALIC = true;
                        } else if ((color.equals(ChatColor.MAGIC)) && (!MAGIC)) {
                            MAGIC = true;
                        } else if ((color.equals(ChatColor.STRIKETHROUGH)) && (!STRIKETHROUGH)) {
                            STRIKETHROUGH = true;
                        } else if ((color.equals(ChatColor.UNDERLINE)) && (!UNDERLINE)) {
                            UNDERLINE = true;
                        }
                    } else if ((textColor == null) && (color.isColor())) {
                        textColor = color;
                    }
                }
            } else if ((index > 0) && (!PASSED)) {
                char c = prefix.charAt(index);
                char c1 = prefix.charAt(index - 1);
                if ((c != '§') && (c1 != '§') && (c != ' ')) {
                    PASSED = true;
                }
            }
            if ((!PASSED) && (prefix.charAt(index) != ' ')) {
                prefix.deleteCharAt(index);
            }
            if (textColor != null) {
                break;
            }
        }
        String result = suffix.toString().isEmpty() ? "" : getResult(BOLD, ITALIC, MAGIC, STRIKETHROUGH, UNDERLINE, textColor);
        if ((!suffix.toString().isEmpty()) && (!suffix.toString().startsWith("§"))) {
            suffix.insert(0, result);
        }
        return new String[]{
                prefix.toString().length() > 16 ? prefix.toString().substring(0, 16) : prefix.toString(),
                suffix.toString().length() > 16 ? suffix.toString().substring(0, 16) : suffix.toString()
        };
    }

    private static String getResult(boolean BOLD, boolean ITALIC, boolean MAGIC, boolean STRIKETHROUGH, boolean UNDERLINE, ChatColor color) {
        return ((color != null) && (!color.equals(ChatColor.WHITE)) ? color : "") + "" + (BOLD ? ChatColor.BOLD : "") +
                (ITALIC ? ChatColor.ITALIC : "") + (MAGIC ? ChatColor.MAGIC : "") + (STRIKETHROUGH ? ChatColor.STRIKETHROUGH : "") +
                (UNDERLINE ? ChatColor.UNDERLINE : "");
    }

    public static String substringToMax(int characters, String string) {
        if (color(string).length() > characters) {
            return string.substring(0, characters);
        }
        return color(string);
    }

    public static String color(String str) {
        return ChatColor.translateAlternateColorCodes('&', str);
    }
}