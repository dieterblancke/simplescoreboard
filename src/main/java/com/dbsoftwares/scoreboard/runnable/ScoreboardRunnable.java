package com.dbsoftwares.scoreboard.runnable;

/*
 * Created by DBSoftwares on 08/01/2018
 * Developer: Dieter Blancke
 * Project: CExtensions
 */

import com.dbsoftwares.centrixcore.api.CCore;
import com.dbsoftwares.centrixcore.api.user.User;
import com.dbsoftwares.scoreboard.ScoreboardManager;
import com.dbsoftwares.scoreboard.scoreboard.Scoreboard;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ScoreboardRunnable implements Runnable {

    @Override
    public void run() {
        ScoreboardManager instance = ScoreboardManager.getInstance();
        List<String> toRemove = Lists.newArrayList();

        for (Map.Entry<String, Scoreboard> entry : instance.getScoreboards().entrySet()) {
            Optional<User> optional = CCore.getApi().getUser(entry.getKey());
            if (!optional.isPresent()) {
                toRemove.add(entry.getKey());
                continue;
            }
            User user = optional.get();
            Scoreboard scoreboard = entry.getValue();

            for (Scoreboard board : instance.getScoreboards().values()) {
                board.updateHealthBar(user.getName(), user.getPlayer().getHealth());
            }

            scoreboard.updateHealthBar();
            scoreboard.updateText();
        }

        toRemove.forEach(name -> instance.getScoreboards().remove(name));
    }
}