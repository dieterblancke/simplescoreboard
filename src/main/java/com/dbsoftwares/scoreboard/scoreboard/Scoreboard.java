package com.dbsoftwares.scoreboard.scoreboard;

/*
 * Created by DBSoftwares on 08/01/2018
 * Developer: Dieter Blancke
 * Project: CExtensions
 */

import com.dbsoftwares.centrixcore.api.placeholders.PlaceHolderAPI;
import com.dbsoftwares.centrixcore.api.user.User;
import com.dbsoftwares.centrixcore.api.utils.Utils;
import com.dbsoftwares.scoreboard.ScoreboardManager;
import com.dbsoftwares.scoreboard.interfaces.IScoreboard;
import com.dbsoftwares.scoreboard.util.Utils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

import java.util.LinkedList;
import java.util.Map;

@Data
public class Scoreboard implements IScoreboard {

    private User user;
    private org.bukkit.scoreboard.Scoreboard scoreboard;
    private ScoreboardSettings settings;

    private LinkedList<ScoreboardLine> lines = Lists.newLinkedList();
    private Map<Team, String> teams = Maps.newHashMap();

    public Scoreboard(User user, ScoreboardSettings settings) {
        this.user = user;
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.settings = settings;

        if (settings.isSidebarEnabled()) {
            Objective objective = scoreboard.registerNewObjective("customsidebar", "dummy");
            objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        }

        if (settings.isHealthbarEnabled()) {
            Objective objective = scoreboard.registerNewObjective("customhealthbar", "dummy");
            objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
        }
    }

    @Override
    public void buildScoreboard() {
        if (settings.isHealthbarEnabled()) {
            Objective objective = scoreboard.getObjective(DisplaySlot.BELOW_NAME);

            updateHealthBar();
        }

        if (settings.isSidebarEnabled()) {
            Objective objective = scoreboard.getObjective(DisplaySlot.SIDEBAR);
            objective.setDisplayName(Utils.format(settings.getTitle()));

            Integer i = 15;
            for (String line : settings.getLines()) {
                if (i < 0) {
                    break;
                }
                Team team = scoreboard.registerNewTeam("Team" + i);
                String color = Utils.colorList.get(i - 1);
                team.addEntry(color);

                ScoreboardLine scoreboardLine = new ScoreboardLine(team, line, i);
                lines.add(scoreboardLine);

                scoreboardLine.update();
                setScore(objective, color, i);
                i--;
            }
        }

        Team team = scoreboard.registerNewTeam("NOPUSH");
        team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        team.addEntry(user.getName());

        user.getPlayer().setScoreboard(scoreboard);

        ScoreboardManager.getInstance().getScoreboards().put(user.getName(), this);
    }

    @Override
    public void updateText() {
        if (!settings.isSidebarEnabled()) {
            return;
        }
        for (ScoreboardLine line : lines) {
            line.update();
        }
    }

    @Override
    public void updateTitle() {
        if (!settings.isSidebarEnabled()) {
            return;
        }
        Objective objective = scoreboard.getObjective(DisplaySlot.SIDEBAR);
        objective.setDisplayName(Utils.format(settings.getTitle()));
    }

    @Override
    public void unregister() {
        ScoreboardManager.getInstance().getScoreboards().remove(user.getName());
        scoreboard.getObjectives().forEach(Objective::unregister);
        scoreboard.getTeams().forEach(Team::unregister);
    }

    @Override
    public void updateHealthBar() {
        updateHealthBar(user.getName(), user.getPlayer().getHealth());
    }

    @Override
    public void updateHealthBar(String name, Number health) {
        if (!settings.isHealthbarEnabled()) {
            return;
        }
        Objective objective = scoreboard.getObjective(DisplaySlot.BELOW_NAME);
        objective.setDisplayName(Utils.format("&4❤"));
        setScore(objective, name, health);
    }

    private String formatLine(String line) {
        return Utils.format(PlaceHolderAPI.formatMessage(user, line.isEmpty() ? Utils.randomColoredString(3) : line));
    }

    private void setScore(Objective objective, String text, Number score) {
        objective.getScore(Utils.format(text)).setScore(score.intValue());
    }
}