package com.dbsoftwares.scoreboard.scoreboard;

/*
 * Created by DBSoftwares on 08/01/2018
 * Developer: Dieter Blancke
 * Project: CExtensions
 */

import com.dbsoftwares.centrixcore.api.extension.Extension;
import com.dbsoftwares.centrixcore.api.language.Language;
import com.dbsoftwares.scoreboard.ScoreboardManager;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor @AllArgsConstructor
public class ScoreboardSettings {

    private boolean sidebarEnabled;
    private boolean healthbarEnabled;
    private String title;
    private List<String> lines;

    public static ScoreboardSettings getDefault() {
        ScoreboardManager instance = Extension.getPlugin(ScoreboardManager.class);
        return new ScoreboardSettings(true, true, instance.getTitle(Language.ENGLISH), instance.getLines(Language.ENGLISH));
    }
}