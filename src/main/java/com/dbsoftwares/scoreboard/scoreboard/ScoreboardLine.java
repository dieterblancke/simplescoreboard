package com.dbsoftwares.scoreboard.scoreboard;

/*
 * Created by DBSoftwares on 08/01/2018
 * Developer: Dieter Blancke
 * Project: CExtensions
 */

import com.dbsoftwares.centrixcore.api.utils.Utils;
import com.dbsoftwares.scoreboard.util.Utils;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.scoreboard.Team;

@Data
@AllArgsConstructor
public class ScoreboardLine {

    private Team team;
    private String text;
    private int score;

    public void update() {
        String[] splitted = Utils.splitString(text);
        String prefix = Utils.format(splitted[0]);
        String suffix = Utils.format(splitted[1]);

        team.setPrefix(prefix.length() > 16 ? prefix.substring(0, 16) : prefix);
        team.setSuffix(suffix.length() > 16 ? Utils.substringToMax(16, suffix) : suffix.substring(0, suffix.length()));
    }
}