package com.dbsoftwares.scoreboard.interfaces;

/*
 * Created by DBSoftwares on 08/01/2018
 * Developer: Dieter Blancke
 * Project: CExtensions
 */

public interface IScoreboard {

    void buildScoreboard();

    void updateText();

    void updateTitle();

    void unregister();

    void updateHealthBar();

    void updateHealthBar(String name, Number health);

}